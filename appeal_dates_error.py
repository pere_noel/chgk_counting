import requests
import datetime as dt


def do():
	
	date_start_after = '2022-12-22'
	items = 1000
	rating_yes = 'properties.maiiRating=true'

	url = f'https://api.rating.chgk.net/tournaments?page=1&itemsPerPage={items}&dateStart%5Bafter%5D={date_start_after}&{rating_yes}'
	r = requests.get(url)

	data = r.json()

	for t in data:
		if (t['type']['id'] == 3):
			compile_appeal_dates(t)


def compile_appeal_dates(tournament):
	date_end = convert_time(tournament['dateEnd'][:-6])
	appeal_deadline = convert_time(tournament['synchData']['dateAppealAllowedTo'][:-6])

	if (date_end > appeal_deadline):
		print(tournament['name'])
		print(f"https://rating.chgk.info/tournament/{tournament['id']}")


def convert_time(string):
	return dt.datetime.strptime(string, '%Y-%m-%dT%H:%M:%S')


if __name__ == '__main__':
    do()
